import constants from "../../assets/constants.json";

export default function ChangeDirection(lang) {
  console.log(lang);
  console.log(constants.languages.Arabic);
  document
    .getElementsByTagName("html")[0]
    .setAttribute("dir", lang === constants.languages.Arabic ? "rtl" : "ltr");
  document
    .getElementsByTagName("html")[0]
    .setAttribute("lang", lang === constants.languages.Arabic ? "ar" : "en");
}
