import { createTransformer } from "../../server/transformer";

const SigninForward = {
  o: [
    ["username", "username"],
    ["password", "password"]
  ]
};
export default createTransformer(SigninForward);
