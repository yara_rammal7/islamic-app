import { createTransformer } from "../../server/transformer";

const userInfo = {
  o: [
    ["email", "email"],
    ["firstName", "first_name"],
    ["lastName", "last_name"],
    ["isActive", "is_Active"]
  ]
};

const SigninBackward = {
  o: [
    ["eUser", "e_user"],
    ["token", "token"],
    ["userInfo", "user_info", userInfo]
  ]
};
export default createTransformer(SigninBackward);
