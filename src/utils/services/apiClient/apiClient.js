import axios from "axios";
import store from "../../../redux/store";
import { unauthUser, changeAuth } from "../../../redux/auth/actions";
import moment from "moment";

import backend_url from "../../../environment";

export const baseDomain = backend_url;

const url = "/procurement/api";
export default function apiClient(parameters) {
  var date = new Date();
  let todayDate = moment(date, "ddd MMM DD YYYY HH:mm:ss");
  var dateNow = todayDate.format("YYYY-MM-DDTHH:mm");
  var x = moment(dateNow, "YYYY-MM-DDTHH:mm");

  var changeExpiryDate = moment.utc(
    store.getState().user.expiresAt,
    "DD/MM/YYYY HH:mm:ss"
  );
  var formatExpiryDate = changeExpiryDate.local().format("YYYY-MM-DDTHH:mm");
  var y = moment(formatExpiryDate, "YYYY-MM-DDTHH:mm");

  let difference = y.diff(x, "minutes");

  const { authorize, method, resource, body, cancelToken } = parameters;
  if (authorize) {
    if (difference > 5) {
      if (method !== "GET") {
        return axios({
          method: method,
          url: baseDomain + url + resource,
          headers: {
            Authorization: "Bearer " + store.getState().user.accessToken,
            "Content-Type": "application/json",
          },
          data: body,
          cancelToken: cancelToken,
        }).catch((error) => {
          console.log("here", error.response);
          let code = error.response.data.code;
          let err = new Error("Error");
          err.code = error.response.data.code;

          if (code === 16) {
            store.dispatch(unauthUser());
          } else {
            if (
              code === 0 ||
              code === 4 ||
              code === 5 ||
              code === 6 ||
              code === 7 ||
              code === 8 ||
              code === 12 ||
              code === 13 ||
              code === 14 ||
              code === 15 ||
              code === 17
            ) {
              err.message = "errorGeneral";
            } else if (code === 2) {
              err.message = "errorPermission";
            } else if (code === 3) {
              err.message = "errorRequired";
            } else {
              err.message = null;
            }
            throw err;
          }
        });
      } else {
        return axios({
          method: method,
          url: baseDomain + url + resource,
          headers: {
            Authorization: "Bearer " + store.getState().user.accessToken,
          },
          params: body,
          cancelToken: cancelToken,
        }).catch((error) => {
          console.log("here", error.response);
          let code = error.response.data.code;
          let err = new Error("Error");
          err.code = error.response.data.code;

          if (code === 16) {
            store.dispatch(unauthUser());
          } else {
            if (
              code === 0 ||
              code === 4 ||
              code === 5 ||
              code === 6 ||
              code === 7 ||
              code === 8 ||
              code === 12 ||
              code === 13 ||
              code === 14 ||
              code === 15 ||
              code === 17
            ) {
              err.message = "errorGeneral";
            } else if (code === 2) {
              err.message = "errorPermission";
            } else if (code === 3) {
              err.message = "errorRequired";
            } else {
              err.message = null;
            }
            throw err;
          }
        });
      }
    } else {
      return axios({
        method: "POST",
        url: baseDomain + url + "/refresh_token/",
        data: { refresh: store.getState().user.refreshToken },
      })
        .then((response) => {
          console.log("here", response);

          store.dispatch(
            changeAuth(
              response.data.access,
              response.data.refresh,
              response.data.expires_at
            )
          );

          return apiClient(parameters);
        })
        .catch((e) => {
          store.dispatch(unauthUser());
          console.log("catch refresh token", e.response);
        });
    }
  } else {
    if (method !== "GET") {
      return axios({
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
        url: baseDomain + url + resource,
        data: body,
        cancelToken: cancelToken,
      });
    } else {
      return axios({
        method: method,
        url: baseDomain + url + resource,
        params: body,
        cancelToken: cancelToken,
      });
    }
  }
}
