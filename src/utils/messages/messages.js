import Messages from "../../containers/Messages";
const messages = {
  en: {
    ...Messages.en
  },
  ar: {
    ...Messages.ar
  }
};
export default messages;
