import React, { useEffect } from "react";
import { Route, Redirect } from "react-router-dom";
// import { connect } from "react-redux";
import AUX from "./hoc/Aux_";
import { IntlProvider } from "react-intl";
import messages from "../src/utils/messages/messages";
import ChangeDirection from "./utils/changeDirection/ChangeDirection.js";
import { injectIntl } from "react-intl";
import LoadingProgress from "react-js-loading-progress-bar";
import { connect } from "react-redux";

const routesRestricted = [
  // {
  //   path: "categories",
  //   component: React.lazy(() => import("./containers/Categories/Categories")),
  // },
];

const routes = [
  {
    path: "",
    component: React.lazy(() => import("./containers/Signin/Signin")),
  },
  {
    path: "activities",
    component: React.lazy(() => import("./containers/Activities/Activities")),
  },
  {
    path: "categories",
    component: React.lazy(() => import("./containers/Categories/Categories")),
  },
];

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);
const mapStateToProps = (state) => {
  // console.log("state", state);
  return {
    user: state.user,
    lang: state.lang.lang,
  };
};
const AppRouter = (props) => {
  // const { url } = props;

  useEffect(() => {
    ChangeDirection(props.lang);
  }, [props.lang]);
  return (
    <AUX>
      <IntlProvider locale={props.lang} messages={messages[props.lang]}>
        {routes.map((singleRoute) => (
          <React.Suspense
            key={singleRoute.path}
            fallback={
              <div>
                <LoadingProgress
                  useSpinner
                  active={true}
                  total={100}
                  current={75}
                />
              </div>
            }
          >
            <Route
              exact
              path={`/${singleRoute.path}`}
              key={singleRoute.path}
              component={singleRoute.component}
            />
          </React.Suspense>
        ))}
        {/* {console.log("is logged in", props.user.loggedin)} */}
        {routesRestricted.map((singleRoute) => (
          <React.Suspense
            key={singleRoute.path}
            fallback={
              <div>
                <LoadingProgress
                  visualOnly
                  useSpinner
                  active={true}
                  total={100}
                  current={75}
                />
              </div>
            }
          >
            <RestrictedRoute
              exact
              path={`/${singleRoute.path}`}
              key={singleRoute.path}
              component={singleRoute.component}
              isLoggedIn={props.user.loggedin}
            />
          </React.Suspense>
        ))}
      </IntlProvider>
    </AUX>
  );
};
export default connect(mapStateToProps)(injectIntl(AppRouter));
