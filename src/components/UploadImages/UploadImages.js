import React, { useEffect } from "react";
import { injectIntl } from "react-intl";
import Translate from "../../translate";
import "./UploadImages.scss";
import ImgsViewer from "react-images-viewer";
function UploadImages(props) {
  const [thumb, setThumb] = React.useState(null);
  const [openImg, setOpenImg] = React.useState(false);
  const provokeInputFile = () => {
    document.getElementsByClassName("addFile")[0].click();
  };
  const saveDocumentName = (value) => {
    const file = value.target.files[0];
    if (file !== undefined) {
      props.setFile(file);
      props.setDisableBtn(false);
      console.log("file", file);
      props.setNameOfDocument(file.name);
      //   setcheckIfAdd(true);
    }
  };

  useEffect(() => {
    console.log("documents", props.documents);
  }, [props.documents]);

  const openImgs = (value) => {
    setOpenImg(true);
    setThumb(value.blob);
  };
  return (
    <div className="upload-box">
      <div className="form-group">
        <small
          id="typelHelp"
          className="form-text text-muted smallText required"
        >
          {Translate(props, "addDocuments")}
        </small>
        {openImg ? (
          <div className="document">
            <img className="image-big" src={thumb} alt="" />
          </div>
        ) : null}
        {/* <div className="document">
          <div className="button-doc" onClick={provokeInputFile}>
            upload
            <input
              type="file"
              onChange={saveDocumentName}
              className="addFile"
              required
              // onKeyDown={handleEnterPress}
            />
          </div>
        </div> */}
        <div className="row img-div">
          <div
            className="col-lg-3 col-md-3 col-sm-12 col-12 button-div-plus"
            onClick={provokeInputFile}
          >
            <input
              type="file"
              onChange={saveDocumentName}
              className="addFile"
              required
              // onKeyDown={handleEnterPress}
            />
          </div>

          {props.documents.length > 0
            ? props.documents.map((value, index) => (
                <div className="col-lg-3 col-md-3 col-sm-12 col-12 ">
                  <img
                    className="img-size"
                    key={index}
                    src={value.blob}
                    alt=""
                    onClick={() => openImgs(value)}
                  />{" "}
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}
export default injectIntl(UploadImages);
