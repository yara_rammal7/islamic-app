import React, { Fragment } from "react";
import Cropper from "react-easy-crop";
import Button from "@material-ui/core/Button";
// import ImgDialog from "./ImgDialog";
import getCroppedImg from "./cropImage";
import "./styles.scss";
import Translate from "../../translate";
import { injectIntl } from "react-intl";

import { baseDomain } from "../../utils/services/apiClient/apiClient";

function Crop(props) {
  const [crop, setCrop] = React.useState({ x: 0, y: 0 });
  const [rotation, setRotation] = React.useState(0);
  const [zoom, setZoom] = React.useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = React.useState(null);
  const [croppedImage, setCroppedImage] = React.useState(null);
  const [imageSrc, setImageSrc] = React.useState(null);
  const [showImage, setShowImage] = React.useState(false);
  const [showCropped, setShowcropped] = React.useState(false);
  const [random, setRandom] = React.useState(null);

  const onCropComplete = React.useCallback((croppedArea, croppedAreaPixels) => {
    setCroppedAreaPixels(croppedAreaPixels);
  }, []);
  const [showPrev, setShowPrev] = React.useState(
    props.showPrev !== null ? false : true
  );
  const showCroppedImage = React.useCallback(async () => {
    try {
      const cropImage = await getCroppedImg(
        imageSrc,
        croppedAreaPixels,
        rotation
      );
      console.log("donee", cropImage);
      setShowcropped(true);
      setCroppedImage(URL.createObjectURL(cropImage));

      const min = 1;
      const max = 100;
      const rand = min + Math.random() * (max - min);
      setRandom(random + rand);
      const random2 = random + rand;
      console.log("random2", random2);
      const fileName = "newFile" + random2 + ".jpg";
      const file = new File([cropImage], fileName, {
        type: "image/jpeg",
      });
      props.onSelect(file);
    } catch (e) {
      console.error(e);
    }
  }, [props, imageSrc, croppedAreaPixels, rotation, random]);
  // const onClose = React.useCallback(() => {
  //   setCroppedImage(null);
  // }, []);
  const onFileChange = async (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      let imageDataUrl = await readFile(file);
      // console.log("imafe", imageDataUrl);
      setImageSrc(imageDataUrl);
      setCrop({ x: 0, y: 0 });
      setZoom(1);
      setShowImage(true);
    }
  };
  // const cropSize = {
  //   width: 100,
  //   height: 200
  // };

  const handleEnterPress = (e) => {
    if (e.keyCode === 13) {
      handleNext(e);
    }
  };

  const provokeInputFile = () => {
    document.getElementsByClassName("addFile")[0].click();
  };

  const handleNext = (e) => {
    e.preventDefault();
    var validate = document.getElementById("form-document").checkValidity();
    console.log("validate", validate);
  };
  const onEdit = () => {
    setShowcropped(false);
  };

  const onDelete = () => {
    console.log("here", showPrev);
    setShowcropped(false);
    setShowImage(false);
    setShowPrev(true);
  };

  return (
    <div className="cropImageDiv">
      {showPrev ? (
        <>
          {showImage === false && showCropped === false ? (
            <div className="form-group">
              <div className="searchBtnDiv">
                <small
                  id="typelHelp"
                  className="form-text text-muted smallText required"
                >
                  {Translate(props, "document")}
                </small>
                <div style={{ display: "flex", position: "relative" }}>
                  <div
                    className="form-control uploadImage"
                    // onClick={provokeInputFile}
                  >
                    <button
                      className="Upload_button Raleway-font mt-2"
                      onClick={provokeInputFile}
                    >
                      {Translate(props, "UploadImage")}
                    </button>
                  </div>
                  <input
                    type="file"
                    onChange={onFileChange}
                    className="addFile"
                    required
                    onKeyDown={handleEnterPress}
                  />
                </div>
              </div>
            </div>
          ) : showCropped === false && showImage === true ? (
            <>
              {imageSrc && (
                <Fragment>
                  <div className="cropContainer">
                    <Cropper
                      image={imageSrc}
                      crop={crop}
                      rotation={rotation}
                      zoom={zoom}
                      aspect={2 / 2}
                      // cropSize={cropSize}
                      onCropChange={setCrop}
                      onRotationChange={setRotation}
                      onCropComplete={onCropComplete}
                      onZoomChange={setZoom}
                    />
                  </div>
                  <Button
                    onClick={showCroppedImage}
                    variant="contained"
                    color="primary"
                    classes={{ root: "cropButton" }}
                  >
                    Show Result
                  </Button>
                  {/* <ImgDialog img={croppedImage} onClose={onClose} /> */}
                </Fragment>
              )}
            </>
          ) : null}
          {showCropped ? (
            <>
              <div className="imgContainer">
                <img src={croppedImage} alt="" className="img" />
              </div>
              <button className="cropEdit" onClick={onEdit}>
                Edit
              </button>
              <button className="cropDelete" onClick={onDelete}>
                Delete
              </button>
            </>
          ) : null}
        </>
      ) : (
        <>
          <div className="imgContainer">
            <img src={baseDomain + props.showPrev} alt="" className="img" />
          </div>
          <button className="cropDelete" onClick={onDelete}>
            Delete
          </button>
        </>
      )}
    </div>
  );
}
function readFile(file) {
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => resolve(reader.result), false);
    reader.readAsDataURL(file);
  });
}

export default injectIntl(Crop);
