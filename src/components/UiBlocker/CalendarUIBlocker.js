import React from "react";
import "./CalendarUIBlocker.scss";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";

function UiBlocker(props) {
  return (
    <div
      // className="CalendarUIBlockerStyle"
      className={
        props.push.push
          ? "CalendarUIBlockerStyle open"
          : "CalendarUIBlockerStyle"
      }
    />
  );
}

const mapStateToProps = state => {
  return {
    push: state.push
  };
};

export default connect(mapStateToProps)(injectIntl(UiBlocker));
