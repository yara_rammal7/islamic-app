import React from "react";
import { ClipLoader } from "react-spinners";

const loader_whole_page = {
  position: "fixed",
  height: "100%",
  width: "100%",
  left: "0%",
  top: "0%",
  backgroundColor: "black",
  opacity: 0.5,
  zIndex: 100000
};
const spinner_whole_page = {
  position: "fixed",
  zIndex: 200000,
  top: "50%",
  left: "50%",
  opacity: 1,
  transform: "translate(-50%,-50%)"
};

export default function Loader() {
  return (
    <div>
      <div style={loader_whole_page} />
      <div style={spinner_whole_page}>
         
        <ClipLoader color={"#ffffff"} />
      </div>
    </div>
  );
}
