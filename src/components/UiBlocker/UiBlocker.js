import React from "react";
const blockerStyle = {
  width: "100vw",
  height: "100vh",
  left: 0,
  position: "absolute",
  zIndex: 100000,
  top: 0,
  backgroundColor: "gray",
  opacity: "0.2"
};
export default function UiBlocker() {
  return <div style={blockerStyle} />;
}
