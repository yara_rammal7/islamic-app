import React from "react";
import { ClipLoader } from "react-spinners";

export default function Loader() {
  return (
    <div>
      <div className="loader_whole_page" />
      <div className="spinner_whole_page">
        <ClipLoader color={"#ffffff"} />
      </div>
    </div>
  );
}
