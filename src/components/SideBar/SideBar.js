import React from "react";
import { injectIntl } from "react-intl";
import "../../assets/Global.scss";
import "./SideBar.scss";
import Translate from "../../translate";
function SideBar(props) {
  const toggleSideBar = () => {
    document.getElementById("sidebar").classList.toggle("active");
  };
  return (
    <div className="sideBar" id="sidebar">
      <div className="toggle-btn" onClick={toggleSideBar}>
        <span></span>
        <span></span>
        <span></span>
      </div>
      <ul>
        <li>Home</li>
        <li>About</li>
        <li>contact</li>
      </ul>
    </div>
  );
}

export default injectIntl(SideBar);
