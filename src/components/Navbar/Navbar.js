import React from "react";
import {
  Navbar,
  Nav
  // NavDropdown
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Navbar.scss";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
// import SignOut from "../../containers/LoginPage/SignOut";
import Translate from "../../translate";
// import { setPush } from "../../redux/toggle/actions";

//https://react-bootstrap.github.io/components/navbar/#navbar-collapse-props
function NavbarSide(props) {
  // const [signout, setSignout] = React.useState(false);
  // const changeSignout = () => {
  //   setSignout(!signout);
  // };

  // const changeSideBar = () => {
  //   props.setPush(!props.push.push);
  // };
  return (
    <div className="NavBarr">
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="light"
        variant="light"
        // onToggle={changeSideBar}
      >
        {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand> */}
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {/* {props.events.sideBar === true ? ( */}
            <>
              <Nav.Link eventKey="1" onClick={() => props.path.push("/orders")}>
                {Translate(props, "ordersMenu")}
              </Nav.Link>

              <Nav.Link
                eventKey="2"
                onClick={() => props.path.push("/categories")}
              >
                {Translate(props, "categoriesMenu")}
              </Nav.Link>
            </>
            {/* ) : null} */}
            <Nav.Link eventKey="4" onClick={() => props.path.push("/brands")}>
              {Translate(props, "brandsMenu")}
            </Nav.Link>

            <Nav.Link eventKey="6" onClick={() => props.path.push("/tags")}>
              {Translate(props, "tagsMenu")}
            </Nav.Link>
            {/* {props.events.sideBar === true ? ( */}
            <Nav.Link eventKey="3" onClick={() => props.path.push("/model")}>
              {Translate(props, "modelMenu")}
            </Nav.Link>
            <Nav.Link eventKey="3" onClick={() => props.path.push("/banners")}>
              {Translate(props, "bannersMenu")}
            </Nav.Link>
            <Nav.Link eventKey="3" onClick={() => props.path.push("/users")}>
              {Translate(props, "usersMenu")}
            </Nav.Link>
            {/* ) : null} */}
            {/* <NavDropdown
              title={props.user.firstName + " " + props.user.lastName}
              id="collasible-nav-dropdown"
            > */}
            {/* <NavDropdown.Item
                onClick={() => props.path.push("/clinicProfile")}
              >
                {Translate(props, "clinicProfile")}
              </NavDropdown.Item> */}
            {/* <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item> */}
            {/* </NavDropdown> */}
            <Nav.Link
              eventKey="7"
              // onClick={() => setSignout(true)}
            >
              {Translate(props, "signout")}
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      {/* {signout ? <SignOut signout={changeSignout} /> : null} */}
    </div>
  );
}
const mapStateToProps = state => {
  return {
    user: state.user,
    push: state.push,
    events: state.events
  };
};
// const mapDispatchToProps = dispatch => {
//   return {
//     setPush: push => {
//       dispatch(setPush(push));
//     }
//   };
// };
export default connect(
  mapStateToProps
  // mapDispatchToProps
)(injectIntl(NavbarSide));
