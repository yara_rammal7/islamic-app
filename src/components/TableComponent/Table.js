import React from "react";
import Translate from "../../translate";
import { injectIntl } from "react-intl";
import "./Table.scss";
function Table(props) {
  const { children } = props;
  return (
    <div className="tableDiv">
      <table className="table">
        <thead className="theadTable">
          <tr className="trTable">
            {props.tableHeader.map((value, index) => (
              <th className="thTable" scope="col" key={index}>
                {Translate(props, value)}
              </th>
            ))}
          </tr>
        </thead>
        {children}
      </table>
    </div>
  );
}

export default injectIntl(Table);
