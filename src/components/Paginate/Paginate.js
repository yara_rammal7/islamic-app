import React, { Component } from "react";
import ReactPaginate from "react-paginate";
import "./Paginate.scss";

export default class Paginate extends Component {
  constructor(props) {
    super(props);
    this.state = { currpage: 0 };
  }

  show_page = page => {
    this.props.pageChange(page.selected + 1);
  };
  render() {
    return (
      <div>
        <ReactPaginate
          forcePage={this.props.page_toload}
          previousLabel={"<"}
          nextLabel={">"}
          breakLabel={"."}
          pageCount={this.props.totalpages}
          marginPagesDisplayed={0}
          pageRangeDisplayed={2}
          onPageChange={this.show_page}
          containerClassName={"pagination justify-content-center mr-2"}
          subContainerClassName={"page-item"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item purple"}
          previousLinkClassName={"page-link"}
          activeClassName={"page-link active-search-pagination"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link  "}
          breakLinkClassName={"page-link d-none"}
        />
      </div>
    );
  }
}
