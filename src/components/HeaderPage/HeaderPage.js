import React from "react";
import { injectIntl } from "react-intl";
import "./HeaderPage.scss";
// import Translate from "../../translate";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
function HeaderPage(props) {
  return (
    <div className="row HeaderPage">
      {/* <div className="col-lg-3 col-md-3 col-sm-13 col-12">
        <p className="titleStyle">
          {Translate(props, props.title) + " "}
          <span className="totalStyle">{props.total}</span>
        </p>
      </div> */}
      <div className="col-lg-9 col-md-9 col-sm-12 col-12">
        <div className="searchDiv">
          <input
            className="form-control popupInput searchInput"
            value={props.search !== null ? props.search : ""}
            onChange={props.changeValue}
            placeholder="Search"
            onKeyDown={props.pressEnter}
          />
          <button
            className="form-control searchButton"
            onClick={props.searchBtn}
          >
            <FontAwesomeIcon
              style={{ fontSize: "1em" }}
              icon={faSearch}
              className="icon"
            />
          </button>
        </div>
      </div>
      {props.addBtn ? (
        <div className="col-lg-3 col-md-3 col-sm-12 col-12">
          <div className="searchBtnDiv">
            <div
              className="categoryIconSizes"
              style={{
                display: "flex",
                float: "right",
              }}
            >
              <img
                className="w-100 h-100"
                src={require("../../assets/icons/categories/add@2x.png")}
                alt="add_category"
                onClick={props.openAddDialog}
              />
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}
export default injectIntl(HeaderPage);
