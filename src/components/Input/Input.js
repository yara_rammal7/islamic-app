import React from "react";
import { injectIntl } from "react-intl";
import "../../assets/Global.scss";
import Translate from "../../translate";
function Input(props) {
  return (
    <div className="form-group">
      <small
        id="typelHelp"
        className={
          props.required
            ? "form-text text-muted smallText required"
            : "form-text text-muted smallText"
        }
        style={props.textStyle}
      >
        {Translate(props, props.name)}
      </small>
      <input
        type={props.type}
        className="form-control"
        style={props.inputStyle}
        placeholder={Translate(props, props.name)}
        value={props.nameState !== null ? props.nameState : ""}
        onChange={props.onChange}
        onKeyDown={props.handleEnterPress}
        required={props.required}
      />
    </div>
  );
}

export default injectIntl(Input);
