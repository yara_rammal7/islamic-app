import React from "react";
import "./Popup.scss";
export default function Popup(props) {
  const { children } = props;
  return (
    <div className="noActionpopupContainer">
      <div className="popup-dropback"></div>
      <div className="popup">
        <div className="closing">
          <img
            className="closeBtn"
            src={require("../../assets/icons/categories/close@2x.png")}
            alt="add_category"
            onClick={props.close}
          />
        </div>
        <div className="popup-subtitle text-center">{children}</div>
      </div>
    </div>
  );
}
