import React from "react";
import "./ActionPopup.scss";
import Translate from "../../translate";
import { injectIntl } from "react-intl";

function ActionPopup(props) {
  const { children } = props;

  return (
    <div>
      <div
        className="popup-dropback"
        onClick={props.outSideClick ? props.close : null}
      ></div>
      <div
        className="popup"
        style={props.width ? { width: props.width } : null}
      >
        <div className="closing">
          <img
            className="closeBtn"
            src={require("../../assets/icons/categories/close@2x.png")}
            alt="add_category"
            onClick={props.close}
          />
        </div>
        <div className="popup-title text-center">
          {Translate(props, props.title)}
        </div>
        <div className="popupContent">
          <div className="divContent">{children}</div>
          {props.btnNumber === 1 ? (
            <button
              className={
                props.disableBtn
                  ? "btn btn-primary btnSubmitDisabled oneButton"
                  : "btn btn-primary btnSubmit oneButton"
              }
              style={props.btnOneStyle}
              onClick={props.submit}
              disabled={props.disableBtn}
            >
              {Translate(props, props.btnMsgOne)}
            </button>
          ) : props.btnNumber === 2 ? (
            <div className="buttonActionPopup">
              <button
                className={
                  props.disableBtn
                    ? "btn btn-primary btnSubmitDisabled floatLeftBtn"
                    : "btn btn-primary btnSubmit floatLeftBtn"
                }
                onClick={props.submit}
                disabled={props.disableBtn}
              >
                {Translate(props, props.btnMsgOne)}
              </button>
              <button
                className={
                  props.disableBtn
                    ? "btn btn-primary btnSubmitDisabled floatRightBtn"
                    : "btn btn-primary btnSubmit floatRightBtn"
                }
                onClick={props.submit}
                disabled={props.disableBtn}
              >
                {Translate(props, props.btnMsgTwo)}
              </button>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}
export default injectIntl(ActionPopup);
