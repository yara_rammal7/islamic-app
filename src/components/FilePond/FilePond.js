// Import React FilePond
import React from "react";
import { FilePond, registerPlugin } from "react-filepond";
// import { baseDomain } from "../../utils/services/apiClient/apiClient";
// Import FilePond styles
import "filepond/dist/filepond.min.css";

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import FilePondPluginImageTransform from "filepond-plugin-image-transform";
import FilePondPluginImageCrop from "filepond-plugin-image-crop";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";

// Register the plugins
registerPlugin(
  FilePondPluginImageExifOrientation,
  FilePondPluginImagePreview,
  FilePondPluginImageTransform,
  FilePondPluginImageCrop
);

// Our app
export default class FileUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      files: []
    };
  }
  componentDidMount = () => {};

  // componentDidUpdate = (prevProps) => {

  //   }
  handleInit() {
    console.log("FilePond instance has initialised");
  }

  render() {
    return (
      <div className="App">
        {this.props.disabled === true ? null : (
          <FilePond
            ref={ref => (this.pond = ref)}
            files={this.state.files}
            allowMultiple={false}
            maxFiles={1}
            oninit={() => this.handleInit()}
            onupdatefiles={fileItems => {
              // Set currently active file objects to this.state
              let ref = this;

              this.setState(
                {
                  files: fileItems.map(fileItem => fileItem.file)
                },
                () => {
                  ref.props.File_Upload(ref.state.files, ref.props.type);
                }
              );
            }}
            // onaddfile = {file => {
            //   console.log(file)
            //   let ref = this;
            //   ref.props.File_Upload(ref.state.files, ref.props.type)}}
            disabled={this.props.disabled}
            labelIdle={"Upload " + this.props.type + " (Drag & Drop)"}
            //  allowImageExifOrientation = {false}
            allowImageCrop={true}
            allowImageTransform={true}
            imageCropAspectRatio={"1:1"}
          />
        )}
      </div>
    );
  }
}
