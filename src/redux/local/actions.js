import constants from "../../assets/constants.json";
import ChangeDirection from "../../utils/changeDirection/ChangeDirection.js";

export function setLang(lang) {
  ChangeDirection(lang);

  return {
    type: constants.reduxActions.addLang,
    payload: {
      lang: lang
    }
  };
}
