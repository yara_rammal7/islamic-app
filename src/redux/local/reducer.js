import constants from "../../assets/constants.json";

export default function reducer(
  state = {
    lang: "en"
  },
  action
) {
  switch (action.type) {
    case constants.reduxActions.addLang: {
      return {
        ...state,
        lang: action.payload.lang
      };
    }
    default:
      return state;
  }
}
