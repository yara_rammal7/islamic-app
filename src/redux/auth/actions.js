import constants from "../../assets/constants.json";

export function authUser(firstName, refreshToken, accessToken, expiresAt) {
  return {
    type: constants.reduxActions.authenticateUser,
    payload: {
      loggedin: true,
      firstName: firstName,
      refreshToken: refreshToken,
      accessToken: accessToken,
      expiresAt: expiresAt,
    },
  };
}

export function unauthUser() {
  return {
    type: constants.reduxActions.unauthenticatUser,
    payload: {
      loggedin: false,
      firstName: null,
      refreshToken: null,
      accessToken: null,
      expiresAt: null,
    },
  };
}
export function changeAuth(accessToken, refreshToken, expiresAt) {
  return {
    type: constants.reduxActions.changeAuth,
    payload: {
      accessToken: accessToken,
      refreshToken: refreshToken,
      expiresAt: expiresAt,
    },
  };
}
