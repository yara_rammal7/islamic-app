import constants from "../../assets/constants.json";

export default function reducer(
  state = {
    loggedin: false,
    firstName: null,
    refreshToken: null,
    accessToken: null,
    expiresAt: null,
  },
  action
) {
  switch (action.type) {
    case constants.reduxActions.authenticateUser: {
      return {
        ...state,
        loggedin: action.payload.loggedin,
        firstName: action.payload.firstName,
        refreshToken: action.payload.refreshToken,
        accessToken: action.payload.accessToken,
        expiresAt: action.payload.expiresAt,
      };
    }
    case constants.reduxActions.unauthenticatUser: {
      return {
        ...state,
        loggedin: action.payload.loggedin,
        firstName: action.payload.firstName,
        refreshToken: action.payload.refreshToken,
        accessToken: action.payload.accessToken,
        expiresAt: action.payload.expiresAt,
      };
    }
    case constants.reduxActions.changeAuth: {
      return {
        ...state,
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken,
        expiresAt: action.payload.expiresAt,
      };
    }

    default:
      return state;
  }
}
