import constants from "../../assets/constants.json";

export function changeBlockUI(blockUI) {
  return {
    type: constants.eventActions.changeBlockUI,
    payload: {
      blockUI: blockUI,
    },
  };
}
export function changePush(push) {
  return {
    type: constants.eventActions.changePush,
    payload: {
      push: push,
    },
  };
}
export function breadcrumb(breadcrumb) {
  return {
    type: constants.eventActions.breadcrumb,
    payload: {
      breadcrumb: breadcrumb,
    },
  };
}