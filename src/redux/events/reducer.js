import constants from "../../assets/constants.json";

export default function reducer(
  state = {
    blockUI: false,
    push: false,
    breadcrumb: [],
  },
  action
) {
  switch (action.type) {
    case constants.eventActions.changeBlockUI: {
      return {
        ...state,
        blockUI: action.payload.blockUI,
      };
    }
    case constants.eventActions.changePush: {
      return {
        ...state,
        push: action.payload.push,
      };
    }
    case constants.eventActions.breadcrumb: {
      return {
        ...state,
        breadcrumb: action.payload.breadcrumb,
      };
    }
    default:
      return state;
  }
}
