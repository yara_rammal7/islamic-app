import { combineReducers } from "redux";
import user from "./auth/reducer";
import lang from "./local/reducer";
import events from "./events/reducer";

export default combineReducers({
  user,
  lang,
  events,
});
