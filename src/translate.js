///Login Container start
function Translate(props, id) {
  return props.intl.formatMessage({ id: id });
}

export default Translate;
