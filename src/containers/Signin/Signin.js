import React, { useEffect } from "react";
import { injectIntl } from "react-intl";
import "./Signin.scss";
import "../../assets/Global.scss";
import Translate from "../../translate";
import Input from "../../components/Input/Input";
import API from "./API";
import { authUser } from "../../redux/auth/actions";
import { changeBlockUI, breadcrumb } from "../../redux/events/actions";

import { connect } from "react-redux";

function Signin(props) {
  const [email, setEmail] = React.useState(null);
  const [password, setPassword] = React.useState(null);
  const [error, setError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");
  const [disabled, setDisabled] = React.useState(true);
  useEffect(() => {
    if (props.user.loggedin === true) {
      props.history.replace("/categories");
    }
  }, [props]);
  const changeEmail = (e) => {
    setError(false);
    setEmail(e.target.value);
  };
  const changePassword = (e) => {
    setError(false);
    setPassword(e.target.value);
  };
  const handleEnterPress = (e) => {
    if (e.keyCode === 13) {
      saveHandler();
    }
  };

  const saveHandler = () => {
    if (
      email !== null &&
      email !== "" &&
      password !== "" &&
      password !== null
    ) {
      SignIn();
    } else {
      setError(true);
      setErrorMessage("Invalid Username or Password");
    }
  };
  useEffect(() => {
    if (email !== null && password !== null) {
      setDisabled(false);
    }
  }, [email, password]);
  const SignIn = () => {
    props.changeBlockUI(true);
    API.login({
      username: email,
      password: password,
    })
      .then((response) => {
        console.log("response", response);
        let x = {
          firstName: response.firstName,
          refreshToken: response.refresh,
          accessToken: response.access,
          expiresAt: response.expiresAt,
        };

        let y = [
          {
            title: "categories",
            path: "/categories",
            clickable: false,
          },
        ];
        props.breadcrumb(y);
        props.changeBlockUI(false);
        props.authUser(x);
      })
      .catch((e) => {
        props.changeBlockUI(false);
        setError(true);
        setErrorMessage("Invalid Username or Password");
        console.log(e);
      });
  };
  return (
    <div className="signin">
      <div className="loginBox">
        <div className="form-group form-check">
          {error ? (
            <div className="alert alert-danger" role="alert">
              {errorMessage}
            </div>
          ) : null}
        </div>
        <h3 className="signinTitle">{Translate(props, "signInTitle")}</h3>
        <h4 className="signinSub">{Translate(props, "signInSub")}</h4>

        <div className="inputValues">
          <Input
            required={false}
            name={"emailAddress"}
            type={"text"}
            nameState={email}
            onChange={changeEmail}
            handleEnterPress={handleEnterPress}
          />
          <Input
            required={false}
            name={"password"}
            type={"password"}
            nameState={password}
            onChange={changePassword}
            handleEnterPress={handleEnterPress}
          />
        </div>

        <div className="buttonSubmit">
          <button
            className={
              disabled
                ? "btn btn-primary btnSubmitDisabled"
                : "btn btn-primary btnSignin"
            }
            onClick={saveHandler}
          >
            {Translate(props, "signIn")}
          </button>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    lang: state.lang,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    authUser: (user) => {
      dispatch(
        authUser(
          user.firstName,
          user.refreshToken,
          user.accessToken,
          user.expiresAt
        )
      );
    },
    changeBlockUI: (bool) => {
      dispatch(changeBlockUI(bool));
    },
    breadcrumb: (breadcrumbArray) => {
      dispatch(breadcrumb(breadcrumbArray));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Signin));
