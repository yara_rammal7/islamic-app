import React, { useEffect } from "react";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import Translate from "../../translate";
import API from "./API";
import HeaderPage from "../../components/HeaderPage/HeaderPage";
import Table from "../../components/TableComponent/Table";
import Paginate from "../../components/Paginate/Paginate";
// import { AddElement, DeleteElement } from "../CommonFunctions";
import "../../assets/Global.scss";
import ActionPopup from "../../components/ActionPopup/ActionPopup";
import Input from "../../components/Input/Input";
import TextArea from "../../components/Input/TextArea";
import { changeBlockUI } from "../../redux/events/actions";
import { breadcrumb } from "../../redux/events/actions";
import UploadImages from "../../components/UploadImages/UploadImages";
import Select from "react-select";

function Activities(props) {
  const [openDialog, setOpenDialog] = React.useState(false);
  const [title, setTitle] = React.useState(null);
  const [description, setDescription] = React.useState(null);

  const [add, setAdd] = React.useState(false);
  const [disableBtn, setDisableBtn] = React.useState(true);

  const [nameOfDocument, setNameOfDocument] = React.useState(null);
  const [file, setFile] = React.useState();
  const [documents, setDocuments] = React.useState([]);

  const [category, setCategory] = React.useState(null);
  const [optionsCategory, setOptionsCategory] = React.useState();

  const [editImgDialog, setEditImgDialog] = React.useState(false);

  const [search, setSearch] = React.useState(null);
  const [pageSearch, setPageSearch] = React.useState(1);
  const [page_toloadSearch, setPage_toLoadSearch] = React.useState(0);

  const searchElements = () => {
    // searchCategories(pageSearch);
  };
  const pressEnter = (e) => {
    if (e.keyCode === 13) {
      //   searchCategories(pageSearch);
    }
  };
  const pageSearchChange = (value) => {
    console.log(value);
    setPageSearch(value);
    // searchCategories(value);
  };

  const changeSearch = (e) => {
    setSearch(e.target.value);
  };
  const getClasses = () => {
    if (props.events.push) return "activities open-SideBar";
    else return "activities closed-SideBar";
  };

  const onClose = () => {
    setOpenDialog(false);
    setAdd(false);
  };
  const openAddDialog = () => {
    setOpenDialog(true);
    setAdd(true);
  };

  const addActivity = () => {};

  const onChange = (value, setState) => {
    setState(value);

    if (setState === setFile) {
      console.log("set file", value);
      let x = { blob: URL.createObjectURL(value), img: value };
      setDocuments((documents) => [x, ...documents]);
    }
  };
  const changeSelect = (value, setState) => {
    setState(value.value);
  };
  useEffect(() => {
    console.log("documents", documents);
  }, [documents]);
  const handleEnterPress = (e) => {
    if (e.keyCode === 13) {
      //   searchCategories(pageSearch);
    }
  };
  return (
    <div className={getClasses()}>
      <div className="box">
        <HeaderPage
          addBtn={true}
          searchBtn={searchElements}
          pressEnter={pressEnter}
          changeValue={changeSearch}
          search={search}
          openAddDialog={openAddDialog}
        />
        <Table
          tableHeader={[
            "image",
            "title",
            "category",
            "description",
            "createdAt",
            "updatedAt",
            "actions",
          ]}
        ></Table>
      </div>
      {editImgDialog ? (
        <ActionPopup
          outSideClick={false}
          close={onClose}
          width={"400px"}
          title={"addActivity"}
          btnNumber={1}
          disableBtn={disableBtn}
          submit={addActivity}
          btnMsgOne={"addActivity"}
        >
          {/* <div className="row justify-content-between"> */}
          {/* <div className="col-lg-5 col-md-5 col-sm-12 col-12">
             
            </div> */}
          {/* <div className="col-lg-5 col-md-5 col-sm-12 col-12"> */}
          <Input
            required={true}
            name={"title"}
            nameState={title}
            type={"text"}
            onChange={(e) => onChange(e.target.value, setTitle)}
            handleEnterPress={handleEnterPress}
          />
          <div className=" inputSearchable form-group">
            <small
              id="typelHelp"
              className={"form-text text-muted smallText required"}
            >
              {Translate(props, "category")}
            </small>
            <Select
              options={optionsCategory}
              onChange={(e) => changeSelect(e, setCategory)}
              placeholder={category !== "" ? category : "Select..."}
              required
            />
          </div>
          <TextArea
            required={true}
            name={"description"}
            nameState={description}
            type={"text"}
            onChange={(e) => onChange(e.target.value, setDescription)}
            handleEnterPress={handleEnterPress}
          />
          {/* </div>
          </div> */}
        </ActionPopup>
      ) : null}
      {openDialog ? (
        <ActionPopup
          outSideClick={false}
          close={onClose}
          width={"400px"}
          title={"editActivityImg"}
          btnNumber={1}
          disableBtn={disableBtn}
          submit={addActivity}
          btnMsgOne={"editActivityImg"}
        >
          <UploadImages
            nameOfDocument={nameOfDocument}
            setFile={(e) => onChange(e, setFile)}
            setDisableBtn={(e) => onChange(e, setDisableBtn)}
            setNameOfDocument={(e) => onChange(e, setNameOfDocument)}
            documents={documents}
          />
        </ActionPopup>
      ) : null}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
    events: state.events,
    lang: state.lang,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(Activities));
