import React from "react";
import { injectIntl } from "react-intl";
import "../assets/Global.scss";
import Input from "../components/Input/Input";
import ActionPopup from "../components/ActionPopup/ActionPopup";
import Table from "../components/TableComponent/Table";
import data from "./try.json";
import HeaderPage from "../components/HeaderPage/HeaderPage";
import "./Trial.scss";

function Trial(props) {
  const [name, setName] = React.useState(null);

  const changeInput = e => {
    setName(e.target.value);
  };
  const onClose = () => {};

  return (
    <div className="box col-lg-12 alignment">
      {/* <ActionPopup
        outSideClick={true}
        close={onClose}
        width={500}
        btnMsgOne={"send"}
        btnMsgTwo={"nosend"}
        title={"Here"}
        btnNumber={1}
        // disableBtn=
      >
        <Input
          name={"input"}
          nameState={name}
          onChange={changeInput}
          required={true}
          // textStyle={{ backgroundColor: "red" }}
          // inputStyle={{ color: "green" }}
          type={"number"}
        />
      </ActionPopup> */}
      <HeaderPage title={"category"} addBtn={true} />
      <Table tableHeader={["id", "gender", "address", "action"]}>
        <tbody className="tbodyTable">
          {data.patients.map((value, index) => (
            <tr className="trTbody" key={index}>
              <td className="tdTbody">{value.id}</td>
              <td className="tdTbody">{value.gender}</td>
              <td className="tdTbody">{value.address}</td>
              <td className="tdTbody">action</td>
            </tr>
          ))}
        </tbody>
      </Table>

      {/* <div className=" mt-md-2 mt-4 mb-2">
        <Paginate
          totalpages={Math.ceil(total / pageSize)}
          pageChange={pageChange}
          page_toload={page_toload}
        />
      </div> */}
    </div>
  );
}

export default injectIntl(Trial);
