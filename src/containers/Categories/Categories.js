import React, { useEffect } from "react";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import Translate from "../../translate";
// import API from "./API";
import HeaderPage from "../../components/HeaderPage/HeaderPage";
import Table from "../../components/TableComponent/Table";
import Paginate from "../../components/Paginate/Paginate";
// import { AddElement, DeleteElement } from "../CommonFunctions";
import "../../assets/Global.scss";
import ActionPopup from "../../components/ActionPopup/ActionPopup";
import Input from "../../components/Input/Input";
import TextArea from "../../components/Input/TextArea";
import { changeBlockUI } from "../../redux/events/actions";
import { breadcrumb } from "../../redux/events/actions";

function Categories(props) {
  const [openDialog, setOpenDialog] = React.useState(false);
  const [category, setCategory] = React.useState(null);
  const [description, setDescription] = React.useState(null);

  const [add, setAdd] = React.useState(false);
  const [disableBtn, setDisableBtn] = React.useState(true);

  const [editImgDialog, setEditImgDialog] = React.useState(false);

  const [search, setSearch] = React.useState(null);
  const [pageSearch, setPageSearch] = React.useState(1);
  const [page_toloadSearch, setPage_toLoadSearch] = React.useState(0);

  const searchElements = () => {
    // searchCategories(pageSearch);
  };
  const pressEnter = (e) => {
    if (e.keyCode === 13) {
      //   searchCategories(pageSearch);
    }
  };
  const pageSearchChange = (value) => {
    console.log(value);
    setPageSearch(value);
    // searchCategories(value);
  };

  const changeSearch = (e) => {
    setSearch(e.target.value);
  };
  const getClasses = () => {
    if (props.events.push) return "categories open-SideBar";
    else return "categories closed-SideBar";
  };

  const onClose = () => {
    setOpenDialog(false);
    setAdd(false);
  };
  const openAddDialog = () => {
    setOpenDialog(true);
    setAdd(true);
  };

  const addCategory = () => {};

  const onChange = (value, setState) => {
    setState(value);
  };

  const handleEnterPress = (e) => {
    if (e.keyCode === 13) {
      //   searchCategories(pageSearch);
    }
  };
  return (
    <div className={getClasses()}>
      <div className="box">
        <HeaderPage
          addBtn={true}
          searchBtn={searchElements}
          pressEnter={pressEnter}
          changeValue={changeSearch}
          search={search}
          openAddDialog={openAddDialog}
        />
        <Table
          tableHeader={["name", "createdAt", "updatedAt", "actions"]}
        ></Table>
      </div>
      {openDialog ? (
        <ActionPopup
          outSideClick={false}
          close={onClose}
          width={"400px"}
          title={"addCategory"}
          btnNumber={1}
          disableBtn={disableBtn}
          submit={addCategory}
          btnMsgOne={"addCategory"}
        >
          <Input
            required={true}
            name={"category"}
            nameState={category}
            type={"text"}
            onChange={(e) => onChange(e.target.value, setCategory)}
            handleEnterPress={handleEnterPress}
          />
        </ActionPopup>
      ) : null}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
    events: state.events,
    lang: state.lang,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {};
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectIntl(Categories));
