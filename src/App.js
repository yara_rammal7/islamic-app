import React from "react";
import "./App.css";
import AppRouter from "./AppRouter.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { IntlProvider } from "react-intl";
// import { connect } from "react-redux";
import messages from "./utils/messages/messages";
import constants from "./assets/constants.json";
import "./assets/scss/SideBar-scss/SideBar.scss";
import SideNav, { NavItem, NavIcon, NavText } from "@trendmicro/react-sidenav";
// import ClickOutside from "./components/UiBlocker//ClickOutside";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";
import "./assets/Global.scss";
import BackgroundUIBlocker from "./components/UiBlocker/BackgroundUIBlocker";
import Navbar from "./components/Navbar/Navbar";
import { connect } from "react-redux";
import Loader from "./components/UiBlocker/Loader";
import UIBlocker from "./components/UiBlocker/UiBlocker";
import { changePush } from "./redux/events/actions";

function App(props) {
  const [expanded, setExpanded] = React.useState(false);
  const [defaultSelectedItem, setDefaultSelectedItem] = React.useState(
    window.location.pathname.split("/")[1]
  );
  // const [signout, setSignout] = React.useState(false);

  // const changeSignout = () => {
  //   // props.signoutPopup(false);
  //   setSignout(!signout);
  // };
  const onSelect = (selected, location, history) => {
    if (selected !== "signout") {
      const to = "/" + selected;
      if (location.pathname !== to) {
        setDefaultSelectedItem(selected);
        history.push(to);
      }
    } else {
      props.signoutPopup(true);
      // setSignout(true);
    }
  };
  const expandedMethod = (expanded) => {
    console.log("expanded", expanded);
    setExpanded(expanded);
    props.changePush(expanded);
  };
  return (
    <div className="App">
      <IntlProvider locale={constants.lang} messages={messages[constants.lang]}>
        <Router>
          <Route
            render={({ location, history }) => (
              <React.Fragment>
                {props.user.loggedin === true ? (
                  <Navbar path={history} />
                ) : null}
                {props.user.loggedin === true ? (
                  <SideNav
                    className="nav-bar"
                    onSelect={(selected) =>
                      onSelect(selected, location, history)
                    }
                    expanded={expanded}
                    onToggle={(expanded) => {
                      expandedMethod(expanded);
                    }}
                  >
                    <SideNav.Toggle />
                    <div
                      className="NavHeader"
                      expanded={expanded}
                      style={
                        expanded ? { display: "block" } : { display: "none" }
                      }
                    >
                      <div className="NavTitle">Side Navigation</div>
                      <div className="NavSubTitle">Styled Side Navigation</div>
                    </div>
                    <SideNav.Nav
                      // defaultSelected={props.events.defaultSideBar}
                      defaultSelected={defaultSelectedItem}
                    >
                      <NavItem eventKey="orders">
                        <NavIcon>
                          {defaultSelectedItem === "orders" ? (
                            <img
                              style={{ width: "20px", height: "25px" }}
                              src={require("./assets/SideBarIcons/ordersColored@2x.png")}
                              alt="edit_category"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "25px" }}
                              src={require("./assets/SideBarIcons/orders@2x.png")}
                              alt="edit_category"
                            />
                          )}
                        </NavIcon>
                        <NavText>Orders</NavText>
                      </NavItem>
                      <NavItem eventKey="categories">
                        <NavIcon>
                          {defaultSelectedItem === "categories" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/categoryColored@2x.png")}
                              alt="edit_category"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/category@2x.png")}
                              alt="edit_category"
                            />
                          )}
                        </NavIcon>
                        <NavText>Categories</NavText>
                      </NavItem>
                      <NavItem eventKey="brands">
                        <NavIcon>
                          {defaultSelectedItem === "brands" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/brandsColored@2x.png")}
                              alt="edit_brands"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/brands@2x.png")}
                              alt="edit_brands"
                            />
                          )}
                        </NavIcon>
                        <NavText>Brands</NavText>
                      </NavItem>
                      <NavItem eventKey="tags">
                        <NavIcon>
                          {defaultSelectedItem === "tags" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/tagsColored@2x.png")}
                              alt="edit_tags"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/tags@2x.png")}
                              alt="edit_tags"
                            />
                          )}
                        </NavIcon>
                        <NavText>Tags</NavText>
                      </NavItem>
                      <NavItem eventKey="model">
                        <NavIcon>
                          {defaultSelectedItem === "model" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/modelColored@2x.png")}
                              alt="edit_model"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/model@2x.png")}
                              alt="edit_model"
                            />
                          )}
                        </NavIcon>
                        <NavText>Model</NavText>
                      </NavItem>
                      <NavItem eventKey="banners">
                        <NavIcon>
                          {defaultSelectedItem === "banners" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/bannersColored@2x.png")}
                              alt="edit_banners"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/banners@2x.png")}
                              alt="edit_banners"
                            />
                          )}
                        </NavIcon>
                        <NavText>Banners</NavText>
                      </NavItem>
                      <NavItem eventKey="users">
                        <NavIcon>
                          {defaultSelectedItem === "users" ? (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/usersColored@2x.png")}
                              alt="edit_users"
                            />
                          ) : (
                            <img
                              style={{ width: "20px", height: "20px" }}
                              src={require("./assets/SideBarIcons/users@2x.png")}
                              alt="edit_users"
                            />
                          )}
                        </NavIcon>
                        <NavText>Users</NavText>
                      </NavItem>
                      <NavItem eventKey="signout">
                        <NavIcon></NavIcon>
                        <NavText>Sign Out</NavText>
                      </NavItem>

                      <div
                        className="BottomLogoMenu"
                        expanded={expanded}
                        style={
                          expanded ? { display: "block" } : { display: "none" }
                        }
                      >
                        <img
                          className="logoImage"
                          src={require("./assets/images/logo@2x.png")}
                          alt="edit_category"
                        />
                      </div>
                    </SideNav.Nav>
                  </SideNav>
                ) : null}
                {/* {signout ? <SignOut signout={changeSignout} /> : null} */}
              </React.Fragment>
            )}
          />

          <Switch>
            <AppRouter />
          </Switch>
        </Router>
      </IntlProvider>
      {props.events.blockUI ? (
        <>
          <UIBlocker /> <Loader />
        </>
      ) : null}
      {expanded ? <BackgroundUIBlocker /> : null}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
    lang: state.lang.lang,
    events: state.events,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    changePush: (bool) => {
      dispatch(changePush(bool));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
